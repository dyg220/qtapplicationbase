#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <list>
#include <initializer_list>
#include <vector>
#include "json.hpp"
#include <iomanip>
#include "include/spdlog/spdlog.h"
#include <QFile>
#include <QStyle>
#include <QDebug>

using namespace std;
using json = nlohmann::json;

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFile file(":/qss/qss/comm.qss");
    if(file.exists()) {
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug() << "文件打开失败";
        } else {
            QString qss = file.readAll();
            setStyleSheet(qss);
        }
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    int i = 10;
    //std::cout << "i=" << i << std::endl;
    std::list<int> list1 = {10, 20, 30, 50};
    std::vector<string> vstr = {"hello", "test", "bed"};

    //std::cout << "size:" << list1.size() << std::endl;
    //std::cout << "vstr:" << vstr[0] << std::endl;
    //system("qpm-cli -v");

    QStringList msg;
    msg << u8"你好";
    msg << u8"你好2";
    msg << u8"你好3";
    for(auto m : msg) {
        ui->txtMsg->append(m);
    }

    //std::cout << std::setw(4) << json::meta() << std::endl;
    //std::string str = R"({"name":"peter","age":23,"married":true})";
    //json json_str = json::parse(str);
    std::string json_str = R"("dir D:\test")";
    spdlog::info("Welcome to spdlog!");
    spdlog::error("Some error message with arg: {}", __FUNCTION__);

    //setMinimumSize(640, 480);
    //auto log_widget = new QTextEdit(this);
    //setCentralWidget(log_widget);
    //int max_lines = 10000; // keep the text widget to max 500 lines. remove old lines if needed.
    //auto logger = spdlog::qt_color_logger_mt("qt_logger", ui->txtLog, max_lines);
    //logger->info("Some info message");



}

